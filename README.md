# Teme završni i diplomski

## 1. Headless wordpress

Wordpress je popularni CMS pisan u PHP-u. Pri razvoju Wordpress stranica developer 
piše i backend i frontend kod. Wordpress također ima i podršku za REST API
koji omogućuje da se Wordpress koristi kao headless CMS. U ovom zadatku potrebno
je instalirati i podesiti Wordpress na hosting paketu (dobit ćete pristup paketu) 
te napraviti jednostavnu web stranicu koja koristi Wordpress kao headless CMS.
Frontend stranice može biti napravljen u Reactu ili Vueu (po izboru), te će 
konzumirati Wordpress REST API kao izvor podataka (backend).


## 2. Serializatori u Django REST frameworku

Django REST framework je popularni Python framework za izradu REST API-ja.
U ovom zadatku potrebno je napraviti jednostavnu web aplikaciju koja će
koristiti Django REST framework za izradu REST API-ja. Aplikacija treba
prikazati mogućnosti korištenja serializatora u Django REST frameworku.


## 3. Serverside dinamičke web stranice primjenom HTMX-a i Alpine.js-a

Predbilježbe: Ana Šenjug, 3. stručni računarstvo

HTMX je biblioteka koja omogućuje dinamičko mijenjanje HTML-a na serverskoj
strani. Alpine.js je biblioteka koja omogućuje dinamičko mijenjanje HTML-a na
klijentskoj strani. U ovom zadatku potrebno je prikazati kako se mogu postići 
dinamične web aplikacije uz minimalnu primjenu klijentskog JavaScripta.


## 4. Aplikacija za dokumentiranje zahtjeva uz pomoć OpenAI 

Predbilježbe: David Jurišić, 3. stručni računarstvo

SRS (Software Requirements Specification) je dokument koji opisuje zahtjeve
za neki softverski sustav. U ovom zadatku potrebno je napraviti aplikaciju
koja će korisniku omogućiti dokumentiranje zahtjeva za neki softverski sustav.
Aplikacija treba koristiti OpenAI API za analizu specifikacije zahtjeva i
upozoriti korisnika na nedostatke u specifikaciji, kontradiktorne zahtjeve,
propuste, te predlagati poboljšanja. Aplikacija treba biti napravljena u
Djangu.


## 5. Web aplikacija za organizaciju baze znanja za chatbot

Chatbot je računalni program koji koristi umjetnu inteligenciju za razgovor
s korisnikom. U ovom zadatku potrebno je napraviti web aplikaciju koja će
korisniku omogućiti organizaciju baze znanja za chatbot. Aplikacija treba
biti napravljena u Djangu. Cilj aplikacije je omogućiti upravljanje bazom 
dokumenata koje mogu uređivati razni korisnici, a chatbot će za generiranje 
odgovora koristiti dokumente iz baze znanja.


